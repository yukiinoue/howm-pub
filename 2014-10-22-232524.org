* [2014-10-22] Maven + Java でファイル読み込み

  src/{main,test}/resources にファイルをつっこんでおく。
  src/{main,test}/test にて、

  String str =
  AClass.class.getClassLoader().getResource("hoge.txt").filePath();

  とできる。
